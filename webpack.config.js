const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

let options = {
    entry: {
        app: "./src/app-index"
        // "electron-app": "./src/electron-index"
    },
    output: {
        filename: "[name].bundle.js",
        // publicPath: path.join(__dirname, "src"),
        path: path.resolve(__dirname, "dist")
    },
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: ["awesome-typescript-loader"]
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: "babel"
            },
            { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: "file-loader" },
            {
                test: /\.(woff|woff2)$/,
                loader: "url-loader?prefix=font/&limit=5000"
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader:
                    "url-loader?limit=10000&mimetype=application/octet-stream"
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url-loader?limit=10000&mimetype=image/svg+xml"
            }
        ]
    },
    resolve: {
        extensions: [".json", ".js", ".jsx", ".css", ".ts", ".tsx"],
        mainFields: [
            "webpack",
            "browser",
            "web",
            "browserify",
            ["jam", "main"],
            "main"
        ]
    },
    node: {
        __dirname: false,
        __filename: false
    },
    devtool: "source-map",
    devServer: {
        publicPath: path.join("dist/")
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: "MSI - drzewo",
            chunks: ["app"]
        })
    ],
    target: "web"
    // target: "electron-main"
};

module.exports = options;
