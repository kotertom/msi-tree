import { createHashHistory } from "history";
import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { Router } from "react-router-dom";
import { routerMiddleware, routerReducer } from "react-router-redux";
import { applyMiddleware, createStore } from "redux";
import { logger } from "redux-logger";
import thunk from "redux-thunk";

import { Application } from "./components/application";
import { answerCurrentQuestion } from "./reducers/answer-current-question";
import { loadTree } from "./reducers/load-tree";
import { rootReducer } from "./utils/root-reducer";

require("../node_modules/bootstrap/dist/css/bootstrap.min.css");
require("../css/style.css");

// const appRoot = d3
//     .select(document.body)
//     .append("div")
//     .attr("id", "app-root")
//     .style("width", "100%")
//     .style("height", "100%")
//     .style("margin", 0)
//     .style("padding", 0)
//     .node() as HTMLDivElement;

const appRoot = document.body.appendChild(document.createElement("div"));
appRoot.setAttribute("id", "app-root");
appRoot.setAttribute(
    "style",
    "width: 100%; height: 100%; margin: 0; padding: 0;"
);

const history = createHashHistory({ hashType: "slash" });

const store = createStore<Application.State>(
    // combineReducers({ loadTree, routerReducer }),
    rootReducer(loadTree, answerCurrentQuestion, routerReducer as any),
    applyMiddleware(logger, routerMiddleware(history), thunk)
);

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <Application />
        </Router>
    </Provider>,
    appRoot
);
