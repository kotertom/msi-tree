import { Application } from "../components/application";
import { Action } from "../actions/index";

type Reducer = (state: Application.State, action: Action) => Application.State;
export function rootReducer(...reducers: Reducer[]) {
    return function(
        state: Application.State,
        action: Action
    ): Application.State {
        return reducers.reduce((acc, reducer) => reducer(acc, action), state);
    };
}
