import { DecisionTree } from "../decision-tree";
import { Application } from "../components/application";
import { Action, ActionTypes } from "../actions/index";

export function answerCurrentQuestion(
    state: Application.State,
    action: Action
): Application.State {
    switch (action.type) {
        case ActionTypes.ANSWER_CURRENT_QUESTION: {
            const { answer } = action;
            const knowledge = state.knowledge.slice().concat({
                question: state.questionQueue[0].question,
                answer
            });
            // sort it
            state.decisionTree.root.evaluate(
                state.questionQueue[0].question,
                state.decisionTree.answers[answer]
            );
            const currentNode = state.questionQueue[0];
            let questionQueue = state.questionQueue
                .slice(1)
                .concat(
                    ...Object.keys(currentNode.answers)
                        .map(a => currentNode.answers[a])
                        .filter(node => node instanceof DecisionTree.ChoiceNode)
                        .map(node => node as DecisionTree.ChoiceNode<string>)
                )
                .sort((a, b) => b.membership - a.membership);
            const itemMaxHeight = Math.max(
                ...questionQueue.map(node => node.membership)
            );
            const bestLeaves = Object.keys(currentNode.answers)
                .map(a => currentNode.answers[a])
                .filter(
                    node =>
                        node instanceof DecisionTree.LeafNode &&
                        node.membership === itemMaxHeight
                );
            if (bestLeaves.length > 0) {
                questionQueue = [];
            }

            return {
                ...state,
                knowledge,
                questionQueue
            };
        }
        default: {
            return state;
        }
    }
}
