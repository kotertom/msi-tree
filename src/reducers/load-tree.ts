import { DecisionTree } from "../decision-tree";
import { Application } from "../components/application";
import { Action, ActionTypes } from "../actions/index";
import { validate } from "jsonschema";
import { readFileSync } from "fs-extra";
import { resolve } from "path";
import * as _ from "lodash";

export function loadTree(
    state: Application.State = Application.initialState,
    action: Action
): Application.State {
    switch (action.type) {
        case ActionTypes.LOAD_TREE: {
            const { decisionTree } = action;

            const questionQueue = [decisionTree.root];

            return {
                ...state,
                decisionTree,
                questionQueue,
                knowledge: []
            };
        }
        default:
            return state;
    }
}
