import { app, BrowserWindow } from "electron";
import { format } from "url";

let window: BrowserWindow;

function createWindow() {
    BrowserWindow.addDevToolsExtension("react-devtools");
    window = new BrowserWindow({
        title: "MSI",
        webPreferences: {
            webSecurity: false
        }
    });
    window.setMenu(null);
    window.maximize();

    const urlToFile = format(`file:/${__dirname}/dist/index.html`);
    window.loadURL(urlToFile);
    console.log("url:", urlToFile);

    window.webContents.openDevTools();

    window.on("closed", () => {
        window = null;
    });
}

app.on("ready", createWindow);

app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});

app.on("activate", () => {
    if (window === null) {
        createWindow();
    }
});
