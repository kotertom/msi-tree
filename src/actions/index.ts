import { DecisionTree } from "../decision-tree";
import { Application } from "../components/application";
import { Dispatch } from "react-redux";

export enum ActionTypes {
    LOAD_TREE = "LOAD_TREE",
    ANSWER_CURRENT_QUESTION = "ANSWER_CURRENT_QUESTION"
}

export type LoadTreeAction = {
    type: ActionTypes.LOAD_TREE;
    decisionTree: DecisionTree<string>;
};

export type AnswerCurrentQuestionAction = {
    type: ActionTypes.ANSWER_CURRENT_QUESTION;
    answer: string;
};

export type Action = LoadTreeAction | AnswerCurrentQuestionAction;

export function loadTreeAction(
    decisionTree: LoadTreeAction["decisionTree"]
): LoadTreeAction {
    return {
        type: ActionTypes.LOAD_TREE,
        decisionTree
    };
}

export function answerCurrentQuestionAction(answer: string) {
    return {
        type: ActionTypes.ANSWER_CURRENT_QUESTION,
        answer
    };
}
