import { validate } from "jsonschema";

const schema = require("../schema.json");

export class DecisionTree<T> {
    constructor(init?: Partial<DecisionTree<T>>) {
        Object.assign(this, init);
    }
    root: DecisionTree.ChoiceNode<T>;
    sets: { [id: string]: DecisionTree.Set<T> };

    answers: { [id: string]: number };

    static fromJson<T>(json: string): DecisionTree<T> {
        const obj: DecisionTree<T> = JSON.parse(json);
        validate(obj, schema);
        const { answers, sets } = obj;
        Object.keys(sets).forEach(feature => {
            sets[feature].membershipFn =
                eval((sets[feature].membershipFn as any) as string) ||
                ((attribute, membership, allAttributes) => membership);
            console.log("fn:", sets[feature].membershipFn);
            sets[feature].items.forEach(item => {
                item.membership = sets[feature].membershipFn(
                    item.attribute,
                    item.membership,
                    sets[feature].items.map(item => item.attribute)
                );
            });
        });
        console.log("price", sets["tanie"].items);
        function buildTree(node: DecisionTree.Node<T>): DecisionTree.Node<T> {
            if (Object.keys(node).indexOf("question") > -1) {
                const choiceNode = node as DecisionTree.ChoiceNode<T>;
                const answers = Object.keys(choiceNode.answers).reduce(
                    (acc, answer) => {
                        acc[answer] = buildTree(choiceNode.answers[answer]);
                        return acc;
                    },
                    {} as DecisionTree.ChoiceNode<T>["answers"]
                );
                return new DecisionTree.ChoiceNode<T>(
                    choiceNode.question,
                    answers
                );
            } else {
                return new DecisionTree.LeafNode<T>(sets, (node as any) as T);
            }
        }
        const root = buildTree(obj.root) as DecisionTree.ChoiceNode<T>;
        return new DecisionTree({
            sets,
            answers,
            root
        });
    }

    leaves(): DecisionTree.LeafNode<T>[] {
        return this.root.leaves();
    }
}

export namespace DecisionTree {
    export type Node<T> = ChoiceNode<T> | LeafNode<T>;

    export class ChoiceNode<T> {
        constructor(
            public question: string,
            public answers: { [id: string]: DecisionTree.Node<T> },
            public membership: number = 1
        ) {}

        evaluate(feature: string, answerValue: number): number {
            const childrenMemberships = Object.keys(this.answers).map(answer =>
                this.answers[answer].evaluate(feature, answerValue)
            );

            return (this.membership = Math.min(
                this.membership,
                Math.max(...childrenMemberships)
            ));
        }

        leaves(): DecisionTree.LeafNode<T>[] {
            return Object.keys(this.answers)
                .map(answer => this.answers[answer])
                .map(node => node.leaves())
                .reduce((acc, leaf) => acc.concat(leaf), []);
        }
    }

    export class LeafNode<T> {
        constructor(
            private sets: DecisionTree<T>["sets"],
            public item: T,
            public membership: number = 1
        ) {}

        evaluate(feature: string, answerValue: number): number {
            const item = this.sets[feature].items.find(
                item => item.item == this.item
            ) || { membership: 0, attribute: 0 };
            const itemMembership = item.membership;
            return (this.membership = Math.min(
                this.membership,
                answerValue * itemMembership +
                    (1 - answerValue) * (1 - itemMembership)
            ));
        }

        leaves() {
            return [this];
        }
    }

    export type Set<T> = {
        items: {
            item: T;
            membership: number;
            attribute?: any;
        }[];
        question: string;
        membershipFn: ((
            attribute: any,
            membership: number,
            allAttributes: any[]
        ) => number);
    };
}
