import { Breadcrumb, BreadcrumbItem } from "react-bootstrap";
import * as React from "react";

export interface AnswerTrailProps {
    items: { text: string; onClick: () => void }[];
}
export function AnswerTrail(props: AnswerTrailProps): JSX.Element {
    const { items } = props;
    return (
        <Breadcrumb>
            {items.map(item => (
                <BreadcrumbItem active>{item.text}</BreadcrumbItem>
            ))}
        </Breadcrumb>
    );
}
