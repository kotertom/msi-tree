import { DecisionTree } from "../decision-tree";
import { Dispatch } from "redux";
import { Application } from "./application";
import { AnswerScreen } from "./answer-screen";
import { connect } from "react-redux";
import { answerCurrentQuestionAction } from "../actions/index";
import { goBack } from "react-router-redux";

function mapStateToProps({
    knowledge,
    ...state
}: Application.State): Partial<AnswerScreen.Props> {
    const currentQuestion =
        state.questionQueue.length > 0
            ? state.decisionTree.sets[state.questionQueue[0].question].question
            : undefined;
    const dtAnswers = state.decisionTree.answers;
    const answers = Object.keys(dtAnswers).sort(
        (a1, a2) => dtAnswers[a2] - dtAnswers[a1]
    );
    const leaves = state.decisionTree
        .leaves()
        .filter(l => l.membership > 0)
        .sort((l1, l2) => l2.membership - l1.membership);
    const displayResult = currentQuestion === undefined;
    return {
        knowledge,
        maxDisplayedElements: 4,
        answers,
        currentQuestion,
        leaves,
        displayResult
    };
}

function mapDispatchToProps(
    dispatch: Dispatch<Application.Context["store"]>
): Partial<AnswerScreen.Props> {
    return {
        onAnswerSelected: answer => {
            dispatch(answerCurrentQuestionAction(answer));
        },
        onResetClicked: () => {
            dispatch(goBack());
        }
    };
}

export const AnswerScreenContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(AnswerScreen);
