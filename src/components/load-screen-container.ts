import { DecisionTree } from "../decision-tree";
import { Application } from "./application";
import { connect, Dispatch } from "react-redux";
import { LoadScreen, InvalidJsonAlert } from "./load-screen";
import { loadTreeAction } from "../actions/index";
import { push } from "react-router-redux";
import { validate, ValidationError } from "jsonschema";
import * as ReactDOM from "react-dom";

const schema = require("../../schema.json");

const mapStateToProps = (state: Application.State) => ({});

const mapDispatchToProps = (
    dispatch: Dispatch<Application.Context["store"]>
): Partial<LoadScreen.Props> => {
    return {
        onFileUpload: content => {
            try {
                const decisionTree = DecisionTree.fromJson<string>(content);
                console.log("tree:", decisionTree);
                dispatch(loadTreeAction(decisionTree));
                dispatch(push("/go"));
            } catch (e) {
                console.info(e);
                if (e instanceof SyntaxError) {
                    alert("The file was not a valid JSON");
                } else if (e instanceof ValidationError) {
                    alert("The file does not match the schema");
                }
            }
        }
    };
};

export const LoadScreenContainer = connect(mapStateToProps, mapDispatchToProps)(
    LoadScreen
);
