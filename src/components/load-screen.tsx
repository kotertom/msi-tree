import * as React from "react";
import { Button, FormControl, Alert } from "react-bootstrap";
// import { readFile } from "fs-extra";

export class LoadScreen extends React.Component<LoadScreen.Props> {
    fileUpload: HTMLInputElement;
    render() {
        return (
            <div
                style={{
                    textAlign: "center",
                    width: "100%",
                    height: "100%",
                    verticalAlign: "middle",
                    display: "flex",
                    alignItems: "center",
                    flexDirection: "column",
                    justifyContent: "center",
                    margin: "auto"
                }}
            >
                <input
                    type="file"
                    ref={ref => {
                        this.fileUpload = ref;
                    }}
                    style={{
                        display: "none"
                    }}
                    accept=".json"
                    onChange={evt => {
                        const file = evt.target.files[0];
                        const reader = new FileReader();
                        reader.onload = function(e: any) {
                            this.props.onFileUpload(e.target.result);
                        }.bind(this);
                        // readFile(file.path, "utf-8").then(
                        //     content => {
                        //         this.props.onFileUpload(content);
                        //     },
                        //     err => {
                        //         console.error("Could not read file", file);
                        //     }
                        // );
                        reader.readAsText(file, "utf-8");
                    }}
                />
                <Button
                    onClick={() => {
                        this.fileUpload.click();
                    }}
                >
                    Load decision tree data...
                </Button>
            </div>
        );
    }
}

export namespace LoadScreen {
    export interface Props {
        onFileUpload: (content: string) => void;
    }
}

export const InvalidJsonAlert = ({ text }: { text: string }) => (
    <Alert>{text}</Alert>
);
