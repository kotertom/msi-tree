import { DecisionTree } from "../decision-tree";
import * as React from "react";
import {
    Breadcrumb,
    Jumbotron,
    ButtonGroup,
    Button,
    Well,
    Badge,
    Label,
    BreadcrumbItem,
    ButtonToolbar
} from "react-bootstrap";
import { Application } from "./application";

export class AnswerScreen extends React.Component<AnswerScreen.Props> {
    render() {
        const {
            answers,
            onAnswerSelected,
            currentQuestion,
            knowledge,
            leaves,
            maxDisplayedElements,
            displayResult,
            onResetClicked
        } = this.props;
        return (
            <div
                className="container"
                style={{
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    justifyContent: "center"
                }}
            >
                <Breadcrumb style={{ width: "60%" }}>
                    {knowledge.map(({ question, answer }) => (
                        <BreadcrumbItem>
                            {question}: {answer}
                        </BreadcrumbItem>
                    ))}
                </Breadcrumb>
                <Jumbotron style={{ minWidth: "60%" }}>
                    <h3 style={{ textAlign: "center" }}>
                        {displayResult
                            ? leaves
                                  .slice(0, 1)
                                  .map(
                                      ({ item, membership }) =>
                                          `Proponuję: ${item} (${membership.toFixed(
                                              2
                                          )})`
                                  )
                            : currentQuestion}
                    </h3>
                    <ButtonToolbar>
                        <ButtonGroup>
                            <Button onClick={onResetClicked}>Reset</Button>
                        </ButtonGroup>
                        {displayResult || (
                            <ButtonGroup style={{ textAlign: "center" }}>
                                {answers.map(answer => (
                                    <Button
                                        onClick={onAnswerSelected.bind(
                                            null,
                                            answer
                                        )}
                                    >
                                        {answer}
                                    </Button>
                                ))}
                            </ButtonGroup>
                        )}
                    </ButtonToolbar>
                </Jumbotron>
                <Well>
                    {leaves
                        .slice(0, maxDisplayedElements)
                        .map(({ item, membership }) => (
                            <Badge>
                                {item} / {membership.toFixed(2)}
                            </Badge>
                        ))}
                    {leaves.length > maxDisplayedElements && <Badge>...</Badge>}
                </Well>
            </div>
        );
    }
}

export namespace AnswerScreen {
    export interface Props {
        maxDisplayedElements: number;
        knowledge: Application.State["knowledge"];
        answers: string[];
        onAnswerSelected: (answer: string) => void;
        onResetClicked: () => void;
        currentQuestion: string;

        leaves: { item: string; membership: number }[];
        displayResult: boolean;
    }
}
