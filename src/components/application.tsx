import { AnswerScreen } from "./answer-screen";
import { LoadScreenContainer } from "./load-screen-container";
import { DecisionTree } from "../decision-tree";
import * as React from "react";
import {
    HashRouter,
    Route,
    Switch,
    RouterChildContext
} from "react-router-dom";
import { Store } from "react-redux";
import * as PropTypes from "prop-types";
import { AnswerScreenContainer } from "./answer-screen-container";
import * as PriorityQueue from "priorityqueuejs";
import { push, RouterState } from "react-router-redux";

export class Application extends React.Component<
    Application.Props,
    Application.State
> {
    context: Application.Context;

    static contextTypes = {
        router: PropTypes.object.isRequired,
        store: PropTypes.object.isRequired
    };

    componentDidMount() {
        const { store } = this.context;
        this.unsubscribe = store.subscribe(() =>
            this.setState(store.getState())
        );
    }

    componentWillUnmount() {
        this.unsubscribe();
    }

    componentDidUpdate(
        prevProps: Application.Props,
        prevState: Application.State
    ) {
        // if (
        //     prevState.decisionTree !== null &&
        //     this.state.decisionTree === null
        // ) {
        //     this.context.store.dispatch(push("/go"));
        // }
    }

    unsubscribe: Function;

    render() {
        console.log("state:", this.state);
        return (
            <Switch>
                <Route exact path="/" component={LoadScreenContainer} />
                <Route exact path="/go" component={AnswerScreenContainer} />
            </Switch>
        );
    }

    static readonly initialState: Application.State = {
        decisionTree: null,
        knowledge: [],
        questionQueue: [],
        location: undefined
    };
}

export namespace Application {
    export interface Props extends React.HTMLProps<any> {}

    // export interface State extends SerializableState, NonserializableState {}

    export interface State extends RouterState {
        decisionTree: DecisionTree<string>;
        knowledge: { question: string; answer: string }[];
        questionQueue: DecisionTree.ChoiceNode<string>[];
    }
    // export interface NonserializableState {}

    export interface Context extends RouterChildContext<any> {
        store: Store<State>;
    }
}
